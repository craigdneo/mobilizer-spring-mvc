package com.inin.mobilizer.servlets;

import com.inin.mobilizer.config.WebConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

public class MobilizerServletInitializer extends
		AbstractAnnotationConfigDispatcherServletInitializer {

	Logger logger = LoggerFactory.getLogger(MobilizerServletInitializer.class);

	@Override
	// Specify @Configuration and/or @Component classes to be provided to the
	// root application context.
	protected Class<?>[] getRootConfigClasses() {
		return null;
	}

	@Override
	// Specify @Configuration and/or @Component classes to be provided to the
	// dispatcher servlet application context.
	protected Class<?>[] getServletConfigClasses() {

		logger.info("::getServletConfigClasses()");

		return new Class<?>[] { WebConfig.class };

	}

	@Override
	// Specify the servlet mapping(s) for the DispatcherServlet, e.g. '/',
	// '/app', etc.
	protected String[] getServletMappings() {

		logger.info("::getServletMappings()");

		return new String[] { "/" };

	}
	
	/**@Override
    protected Filter[] getServletFilters() {
 
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();        
        characterEncodingFilter.setEncoding("UTF-8");
        return new Filter[] { characterEncodingFilter };
    }  **/
}
