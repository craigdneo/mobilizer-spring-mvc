package com.inin.mobilizer.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: CraigD
 * Date: 8/21/13
 * Time: 8:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class ServicesGetPropertyValues {

    public String getStringProperty(String propertyName, String propertyFile)
            throws IOException
    {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propertyFile);
        Properties prop = new Properties();
        prop.load(inputStream);

        if (inputStream == null) {
            throw new FileNotFoundException("property file '" + propertyFile + "' not found in the classpath");
        }

        return prop.getProperty(propertyName);
    }
}
