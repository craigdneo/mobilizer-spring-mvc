package com.inin.mobilizer.service;

import com.inin.mobilizer.models.callbacks.CreateRequest;
import com.inin.mobilizer.models.callbacks.Interaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

public class CallbackServiceImpl implements ICallbackService {

    private static final Logger logger = LoggerFactory.getLogger(CallbackServiceImpl.class);
    private ServicesGetPropertyValues servicesGetPropertyValues;
    private final RestTemplate restTemplate;
    private HttpHeaders defaultHeaders;
    private String BASE_URI = "";
    private String CREATE_URI = "";

    @Autowired
	public CallbackServiceImpl(ServicesGetPropertyValues servicesGetPropertyValues) {

        logger.info("callbackServicImpl::ctor");
        this.servicesGetPropertyValues = servicesGetPropertyValues;
        this.restTemplate = new RestTemplate();

        BASE_URI = getBASE_URI();
        CREATE_URI = getCREATE_URI();

        defaultHeaders = new HttpHeaders();
        defaultHeaders.setContentType(APPLICATION_JSON);
        defaultHeaders.setAccept(singletonList(MediaType.valueOf("application/json")));
    }

    private String getCREATE_URI() {
        try {
            return servicesGetPropertyValues.getStringProperty("CREATE_URI","callback-cloud-service.properties");
        } catch (IOException e) {
            logger.error(e.getStackTrace().toString());
        }
        return null;
    }

    private String getBASE_URI(){
        try {
            return servicesGetPropertyValues.getStringProperty("BASE_URI","callback-cloud-service.properties");
        } catch (IOException e) {
            logger.error(e.getStackTrace().toString());
        }
        return null;
    }

    @Override
    public Interaction createCallback(CreateRequest callbackRequest) {

        if (BASE_URI == null || CREATE_URI == null){
            logger.error("No URIs defined for callback service");
            return null;
        }
        String uri = BASE_URI + CREATE_URI;

        HttpEntity request = new HttpEntity(callbackRequest, defaultHeaders);

        logger.info("Sending createCallback request to Uri:" + uri);

        ResponseEntity<Interaction> callbackInteractionJSON =
                restTemplate.exchange(uri, HttpMethod.POST, request, new ParameterizedTypeReference<Interaction>(){});

        if (callbackInteractionJSON != null){
            logger.info("Created callback interaction with ID:" + callbackInteractionJSON.getBody().getCallback().getCallbackID());
        }
        else{
            logger.warn("No callback interaction created, returning null");
        }


        return callbackInteractionJSON.getBody();

    }
}
