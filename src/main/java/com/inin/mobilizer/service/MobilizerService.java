package com.inin.mobilizer.service;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.WebappContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inin.mobilizer.servlets.MobilizerServletInitializer;

public class MobilizerService {

    /*
        Main entry point of mvc example
        This app creates a grizzly http server on port 8080 to host
        the war file which will enable the creation of callbacks
     */
	public static void main (String[] args) throws Exception {
		
		final Logger logger = LoggerFactory.getLogger(MobilizerService.class);
		
		try{
            final HttpServer server = HttpServer.createSimpleServer("/", 8080);

            // register shutdown hook
            Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
                @Override
                public void run() {
                    logger.info("Stopping server..");
                    server.shutdown();
                }
            }, "shutdownHook"));


			server.start();
			
			MobilizerServletInitializer mobilizerServletInitializer = new MobilizerServletInitializer();
			
			WebappContext webAppContext = new WebappContext("Mobilizer-Service-WebAppContext");
			
			mobilizerServletInitializer.onStartup(webAppContext);
			
			webAppContext.deploy(server);
			
			Thread.currentThread().join();
		}
		catch(Exception ex)
		{
			logger.error(ex.getMessage());
			logger.error(ex.getStackTrace().toString());
			
		}
	}
}
