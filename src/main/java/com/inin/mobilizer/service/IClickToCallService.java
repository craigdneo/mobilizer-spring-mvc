package com.inin.mobilizer.service;

import com.inin.mobilizer.models.clicktocall.ClickToCall;
import com.inin.mobilizer.models.clicktocall.CreateRequest;

public interface IClickToCallService {

    ClickToCall createClickToCall(CreateRequest createRequest);
}
