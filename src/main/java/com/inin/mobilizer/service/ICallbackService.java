package com.inin.mobilizer.service;

import com.inin.mobilizer.models.callbacks.Interaction;
import com.inin.mobilizer.models.callbacks.CreateRequest;

public interface ICallbackService {

    Interaction createCallback(CreateRequest callbackRequest);
}
