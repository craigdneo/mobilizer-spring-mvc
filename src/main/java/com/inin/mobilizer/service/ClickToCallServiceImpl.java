package com.inin.mobilizer.service;

import com.inin.mobilizer.models.clicktocall.ClickToCall;
import com.inin.mobilizer.models.clicktocall.CreateRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

public class ClickToCallServiceImpl implements IClickToCallService {

    private static final Logger logger = LoggerFactory.getLogger(CallbackServiceImpl.class);
    private ServicesGetPropertyValues servicesGetPropertyValues;
    private final RestTemplate restTemplate;
    private HttpHeaders defaultHeaders;
    private String BASE_URI = "";
    private String CREATE_URI = "";


    @Autowired
    public ClickToCallServiceImpl(ServicesGetPropertyValues servicesGetPropertyValues) {

        logger.info("callbackServicImpl::ctor");
        this.servicesGetPropertyValues = servicesGetPropertyValues;
        this.restTemplate = new RestTemplate();

        BASE_URI = getBASE_URI();
        CREATE_URI = getCREATE_URI();

        defaultHeaders = new HttpHeaders();
        defaultHeaders.setContentType(APPLICATION_JSON);
        defaultHeaders.setAccept(singletonList(MediaType.valueOf("application/json")));
    }

    private String getCREATE_URI() {
        try {
            return servicesGetPropertyValues.getStringProperty("CREATE_URI","click-to-call-cloud-service.properties");
        } catch (IOException e) {
            logger.error(e.getStackTrace().toString());
        }
        return null;
    }

    private String getBASE_URI(){
        try {
            return servicesGetPropertyValues.getStringProperty("BASE_URI","click-to-call-cloud-service.properties");
        } catch (IOException e) {
            logger.error(e.getStackTrace().toString());
        }
        return null;
    }

    @Override
    public ClickToCall createClickToCall(CreateRequest createRequest) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
