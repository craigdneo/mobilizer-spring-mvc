package com.inin.mobilizer.controller;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/")
public class MobilizerController {
	
	private static final Logger logger = LoggerFactory.getLogger(MobilizerController.class);
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 * @throws JsonProcessingException 
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	@ResponseBody
	public Callable<String> home() {
		
		logger.info("Starting request on thread" + Thread.currentThread().getName());
		
		return new Callable<String>() {

			@Override
			public String call() throws Exception {
				
				logger.info("Returning response on thread: " + Thread.currentThread().getName());
                return "Mobilizer-service is up and operational.";
			}

		};
	}
}


