package com.inin.mobilizer.controller;

import com.inin.mobilizer.models.callbacks.Interaction;
import com.inin.mobilizer.service.CallbackServiceImpl;
import com.wordnik.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.inin.mobilizer.models.callbacks.CreateRequest;
import com.inin.mobilizer.models.callbacks.CreateResponse;


@Api(value = "callback", description = "callback")
@Controller
@RequestMapping("callback")
public class CallbackController {
	
	private CallbackServiceImpl callbackService;
    private static final Logger logger = LoggerFactory.getLogger(CallbackController.class);
	
	@Autowired
	public CallbackController(CallbackServiceImpl callbackService){

		this.callbackService = callbackService;
        logger.info("CallbackController::ctor");
	}

    @RequestMapping(value="create",  method=RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody CreateResponse createCallback(@RequestBody CreateRequest createRequest){

        logger.info("CallbackController::createCallback");
        Interaction callbackInteraction = callbackService.createCallback(createRequest);
        CreateResponse createResponse = new CreateResponse();
        createResponse.setCallback(callbackInteraction);
        return createResponse;

   }
}
