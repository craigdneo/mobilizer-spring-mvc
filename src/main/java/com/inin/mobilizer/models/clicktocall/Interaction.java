package com.inin.mobilizer.models.clicktocall;

public class Interaction {

    private ClickToCall clickToCall;

    public ClickToCall getClickToCall() {
        return clickToCall;
    }

    public void setClickToCall(ClickToCall clickToCall) {
        this.clickToCall = clickToCall;
    }
}
