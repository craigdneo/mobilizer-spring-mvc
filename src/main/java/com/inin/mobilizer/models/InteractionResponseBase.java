package com.inin.mobilizer.models;

public class InteractionResponseBase {

    private int pollWaitSuggestion;
    private InteractionResponseStatus status;

    public int getPollWaitSuggestion() {
        return pollWaitSuggestion;
    }

    public void setPollWaitSuggestion(int pollWaitSuggestion) {
        this.pollWaitSuggestion = pollWaitSuggestion;
    }

    public InteractionResponseStatus getStatus() {
        return status;
    }

    public void setStatus(InteractionResponseStatus status) {
        this.status = status;
    }
}
