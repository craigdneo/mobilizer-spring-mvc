package com.inin.mobilizer.models.callbacks;

import com.inin.mobilizer.models.InteractionResponseBase;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Callback extends InteractionResponseBase {

    private String participantID;
    private String callbackID;

    public String getParticipantID() {
        return participantID;
    }
    public void setParticipantID(String participantID) {
        this.participantID = participantID;
    }

    public String getCallbackID() {
        return callbackID;
    }

    public void setCallbackID(String callbackID) {
        this.callbackID = callbackID;
    }
}
