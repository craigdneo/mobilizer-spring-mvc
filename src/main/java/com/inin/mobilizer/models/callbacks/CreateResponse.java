package com.inin.mobilizer.models.callbacks;

public class CreateResponse {
	
	private Interaction callback;

	public Interaction getCallback() {
		return callback;
	}

	public void setCallback(Interaction callback) {
		this.callback = callback;
	}

}
