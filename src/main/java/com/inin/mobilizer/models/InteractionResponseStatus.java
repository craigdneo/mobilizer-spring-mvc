package com.inin.mobilizer.models;

public class InteractionResponseStatus {
	
	private String type;
    private String reason;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
