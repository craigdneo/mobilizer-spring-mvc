package com.inin.mobilizer.config.interceptors;

import org.apache.commons.io.input.TeeInputStream;
import org.apache.commons.io.output.TeeOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.DelegatingServletInputStream;
import org.springframework.mock.web.DelegatingServletOutputStream;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.ServletInputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;


public class MobilizerServiceInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(MobilizerServiceInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        logger.info("MobilizerServiceInterceptor.preHandle");

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

        logger.info("MobilizerServiceInterceptor.postHandle");

        HttpServletRequest requestWrapper = loggingRequestWrapper(httpServletRequest);
        logger.info("Request Method:" + requestWrapper.getMethod());

        HttpServletResponse responseWrapper = loggingResponseWrapper((HttpServletResponse) httpServletResponse);
        logger.info("Response ContentType:" + responseWrapper.getContentType());

        super.postHandle(httpServletRequest, httpServletResponse, o, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

        super.afterCompletion(httpServletRequest, httpServletResponse, o,  e);
    }

    private HttpServletRequest loggingRequestWrapper(HttpServletRequest request) {

        return new HttpServletRequestWrapper(request) {

            @Override
            public ServletInputStream getInputStream() throws IOException {

                return new DelegatingServletInputStream(

                        new TeeInputStream(super.getInputStream(), loggingInputStream())
                );
            }
        };
    }

    private HttpServletResponse loggingResponseWrapper(HttpServletResponse response) {

        return new HttpServletResponseWrapper(response) {

            @Override
            public ServletOutputStream getOutputStream() throws IOException {

                return new DelegatingServletOutputStream(

                        new TeeOutputStream(super.getOutputStream(), loggingOutputStream())
                );
            }
        };
    }

    private PrintStream loggingInputStream(){
        return System.out;
    }

    private OutputStream loggingOutputStream() {
        return System.out;
    }


}