package com.inin.mobilizer.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.inin.mobilizer.config.interceptors.MobilizerServiceInterceptor;
import com.inin.mobilizer.service.CallbackServiceImpl;
import com.inin.mobilizer.service.ClickToCallServiceImpl;
import com.inin.mobilizer.service.ServicesGetPropertyValues;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableAspectJAutoProxy
@EnableWebMvc
@ComponentScan(basePackages = {"com.inin.mobilizer"})
public class WebConfig  extends WebMvcConfigurerAdapter {
	
	@Override
	// Configure the HttpMessageConverters to use in argument resolvers and return value handlers 
	// that support reading and/or writing to the body of the request and response.
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters){
		
		ObjectMapper objectMapper = new ObjectMapper();

		objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.disable(SerializationFeature.WRITE_NULL_MAP_VALUES);

		
		// Implementation of HttpMessageConverter that can read and write JSON using Jackson's ObjectMapper.
		// This converter can be used to bind to typed beans, or untyped HashMap instances.
		// By default, this converter supports application/json. 
		// This can be overridden by setting the supportedMediaTypes property.
		//MappingJacksonHttpMessageConverter converter = new MappingJacksonHttpMessageConverter();
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();

        List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();
        supportedMediaTypes.add(MediaType.APPLICATION_JSON);
        jsonConverter.setPrefixJson(false);
        jsonConverter.setSupportedMediaTypes(supportedMediaTypes);
        jsonConverter.setObjectMapper(objectMapper);
		converters.add(jsonConverter);
	}



    @Override
    public void addInterceptors(InterceptorRegistry reg){

        reg.addInterceptor( new MobilizerServiceInterceptor());
    }
	
	@Override
	//Add handlers to serve static resources such as images, js, and, css 
	// files from specific locations under web application root, the classpath, and others.
	public void addResourceHandlers(ResourceHandlerRegistry registry){
		
		registry.addResourceHandler("/**").addResourceLocations("/");
	}
	
	/**@Override
	//Configure asynchronous request handling options.
	public void configureAsyncSupport(AsyncSupportConfigurer configurer){
		
		ThreadPoolTaskExecutor threadPoolExecutor = new ThreadPoolTaskExecutor();
		threadPoolExecutor.setMaxPoolSize(5);
		threadPoolExecutor.setCorePoolSize(3);
		threadPoolExecutor.initialize();
		configurer.setTaskExecutor(threadPoolExecutor);
	}**/
	
	
	
	@Bean
	public RequestMappingHandlerAdapter requestMappingHandlerAdapter() {
		
		final RequestMappingHandlerAdapter handlerAdapter = new RequestMappingHandlerAdapter();
		final MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        List<HttpMessageConverter<?>> list = new ArrayList<HttpMessageConverter<?>>();
		
		List<MediaType> supportedMediaTypes = new ArrayList<MediaType>();

		supportedMediaTypes.add(MediaType.APPLICATION_JSON);
		
		jsonConverter.setPrefixJson(false);

		jsonConverter.setSupportedMediaTypes(supportedMediaTypes);
		
		list.add(jsonConverter);

        String[] supportedHttpMethods = { "POST", "GET", "HEAD" };

        handlerAdapter.setMessageConverters(list);

        handlerAdapter.setSupportedMethods(supportedHttpMethods);


        return handlerAdapter;
	}
	
	@Bean
	public CallbackServiceImpl callbackService(){
		return new CallbackServiceImpl( new ServicesGetPropertyValues());
	}

    @Bean
    public ClickToCallServiceImpl clickToCallService(){
        return new ClickToCallServiceImpl(new ServicesGetPropertyValues());
    }
}
